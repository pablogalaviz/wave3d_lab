

MPI_LIB_PATH = -L/cineca/prod/opt/compilers/intel/pe-xe-2017/binary/impi/2017.3.196/intel64/lib/release_mt -L/cineca/prod/opt/compilers/intel/pe-xe-2017/binary/impi/2017.3.196/intel64/lib 
MPI_INC_PATH =/cineca/prod/opt/compilers/intel/pe-xe-2017/binary/impi/2017.3.196/intel64/include 
MPI_LINK_FLAGS = -Xlinker --enable-new-dtags -Xlinker -rpath -Xlinker /cineca/prod/opt/compilers/intel/pe-xe-2017/binary/impi/2017.3.196/intel64/lib/release_mt -Xlinker -rpath -Xlinker /cineca/prod/opt/compilers/intel/pe-xe-2017/binary/impi/2017.3.196/intel64/lib -Xlinker -rpath -Xlinker /opt/intel/mpi-rt/2017.0.0/intel64/lib/release_mt -Xlinker -rpath -Xlinker /opt/intel/mpi-rt/2017.0.0/intel64/lib -lmpifort -lmpi -lmpigi -ldl -lrt -lpthread

BOOST_LIB_PATH=/cineca/prod/opt/libraries/boost/1.61.0/intelmpi--2017--binary/lib
BOOST_INC_PATH=/cineca/prod/opt/libraries/boost/1.61.0/intelmpi--2017--binary/include

HDF5_LIB_PATH=/cineca/prod/opt/libraries/hdf5/1.8.17/intelmpi--2017--binary/lib
HDF5_INC_PATH=/cineca/prod/opt/libraries/hdf5/1.8.17/intelmpi--2017--binary/include

ZLIB_LIB_PATH=/cineca/prod/opt/libraries/zlib/1.2.8/gnu--6.1.0/lib
ZLIB_INC_PATH=/cineca/prod/opt/libraries/zlib/1.2.8/gnu--6.1.0/include

SZIP_LIB_PATH=/cineca/prod/opt/libraries/szip/2.1/gnu--6.1.0/lib
SZIP_INC_PATH=/cineca/prod/opt/libraries/szip/2.1/gnu--6.1.0/include

SED_FLAGS=-E
CPP	= mpiicpc 
CC	= mpiicc

CPPFLAGS= -qopt-report-phase=vec -qopt-report=2 -qopt-report-filter="wave.cc, 46-55, 67-76, 82-94"  -DINTEL_BUILD
#CPPFLAGS= -qopt-report-phase=vec -qopt-report=2  -DINTEL_BUILD
