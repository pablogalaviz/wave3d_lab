
MPI_COMPILE_FLAGS = $(shell mpicc --showme:compile)
MPI_LINK_FLAGS = $(shell mpicc --showme:link)

BOOST_LIB_PATH=/usr/local/lib
BOOST_INC_PATH=/usr/local/include/boost

HDF5_LIB_PATH=/usr/local/lib
HDF5_INC_PATH=/usr/local/include

SED_FLAGS=-E
CPP	= mpic++ 
CC	= mpicc  
