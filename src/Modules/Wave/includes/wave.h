

// wave.h
//  
// Made by Pablo Galaviz
// e-mail  <aPablo.Galaviz@me.com>
// 



//  This file is part of Wave3d lab
//
//  Wave3d lab is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  Wave3d lab is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with PostNewtonian3BP.  If not, see <http://www.gnu.org/licenses/>.
//



#ifndef WAVE_H

#define WAVE_H

#include<utils.h>

#define M 128*128*128


class Wave
{

  src::severity_logger< severity_level > lg;

  float *field;

  size_t N; 

  template <class T>
  void type1_v(T *A, T* B, T K, int start, int end);
  template <class T>
  void type1_nv(T *A, T* B, T K, int start, int end);

  template <class T>
  void type1v_v(valarray<T> &A, valarray<T> &B, T K, int start, int end);
  template <class T>
  void type1v_nv(valarray<T> &A, valarray<T> &B, T K, int start, int end);

  
  
 public:

  Wave(size_t nx, size_t ny, size_t nz);

  ~Wave(){};

  void update();
  

};


#endif
