

// wave.cc
//  
// Made by Pablo Galaviz
// e-mail  <aPablo.Galaviz@me.com>
// 



//  This file is part of Wave3d lab
//
//  Wave3d lab is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  Wave3d lab is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with PostNewtonian3BP.  If not, see <http://www.gnu.org/licenses/>.
//



#include "wave.h"


Wave::Wave(size_t nx, size_t ny, size_t nz)
{

  field=new  float[nx*ny*nz];
  N=nx*ny*nz;
  
}

/*
 void Wave::vectorized()
{

  BOOST_LOG_SEV(lg, info) << "Starts vectorized loop";

  boost::timer::auto_cpu_timer t;

#ifdef INTEL_BUILD
#pragma vector always
#elif LLVM_BUILD
#pragma clang loop vectorize(enable)
#endif
  for(int j=0; j< 100; j++)
    for(int i=0; i< N; ++i)
      field[i]=sin(i*0.01);
  
  BOOST_LOG_SEV(lg, info) << "Finished vectorized loop";

  
}

void Wave::noVectorized()
{

  BOOST_LOG_SEV(lg, info) << "Starts no vectorized loop";
  
  boost::timer::auto_cpu_timer t;

#ifdef INTEL_BUILD
#pragma novector 
#elif LLVM_BUILD
#pragma clang loop vectorize(disable)
#endif
 for(int j=0; j< 100; j++)
    for(int i=0; i< N; ++i)
      field[i]=sin(i*0.01);
  
  BOOST_LOG_SEV(lg, info) << "Finished no vectorized loop";

  
}
*/

template <class T>
void Wave::type1_v(T *A, T* B, T K, int start, int end) {
  
#pragma vector always
   for (int i = start; i < end; ++i)
    A[i] *= B[i] + K;
}


template <class T>
void Wave::type1_nv(T *A, T* B, T K, int start, int end) {
  
#pragma novector 
  for (int i = start; i < end; ++i)
    A[i] *= B[i] + K;

}

template <class T>
void Wave::type1v_v(valarray<T> &A, valarray<T> &B, T K, int start, int end) {
  
#pragma vector always
   for (int i = start; i < end; ++i)
    A[i] *= B[i] + K;
}


template <class T>
void Wave::type1v_nv(valarray<T> &A, valarray<T> &B, T K, int start, int end) {
  
#pragma novector 
  for (int i = start; i < end; ++i)
    A[i] *= B[i] + K;

}



void Wave::update()
{

    
  int a_i[M] = {1};
  int b_i[M] = {2};

  float a_f[M] = {1.0};
  float b_f[M] = {2.0};

  double a_d[M] = {1.0};
  double b_d[M] = {2.0};

  valarray<double> va_d(1.0,M);
  valarray<double> vb_d(2.0, M);


  {
    BOOST_LOG_SEV(lg, info) << "Type 1 test int vectorized";
    boost::timer::auto_cpu_timer t;
    for(int i =0; i < N; i++)
      type1_v<int>(&a_i[0],&b_i[0],3,0,M);
  }
  

  {
    BOOST_LOG_SEV(lg, info) << "Type 1 test int no vectorized";
    boost::timer::auto_cpu_timer t;
    for(int i =0; i < N; i++)
      type1_nv<int>(&a_i[0],&b_i[0],3,0,M);
  }

  {
    BOOST_LOG_SEV(lg, info) << "Type 1 test valarray<double> vectorized";
    boost::timer::auto_cpu_timer t;
#pragma vector always
    for(int i =0; i < N; i++)
      va_d*=vb_d+3.1493;

  }
  

  {
    BOOST_LOG_SEV(lg, info) << "Type 1 test valarray<double> no vectorized";
    boost::timer::auto_cpu_timer t;
    for(int i =0; i < N; i++)
      type1v_nv<double>(va_d,vb_d,3,0,M);
  }

  
 
  {
    BOOST_LOG_SEV(lg, info) << "Type 1 test float vectorized";
    boost::timer::auto_cpu_timer t;
    for(int i =0; i < N; i++)
      type1_v<float>(&a_f[0],&b_f[0],3.141593,0,M);
  }
  {
    BOOST_LOG_SEV(lg, info) << "Type 1 test float no vectorized";
    boost::timer::auto_cpu_timer t;
    for(int i =0; i < N; i++)
      type1_nv<float>(&a_f[0],&b_f[0],3.141593,0,M);
  }

    {
    BOOST_LOG_SEV(lg, info) << "Type 1 test double vectorized";
    boost::timer::auto_cpu_timer t;
    for(int i =0; i < N; i++)
      type1_v<double>(&a_d[0],&b_d[0],3.141593,0,M);
  }

  {
    BOOST_LOG_SEV(lg, info) << "Type 1 test double no vectorized";
    boost::timer::auto_cpu_timer t;
    for(int i =0; i < N; i++)
      type1_nv<double>(&a_d[0],&b_d[0],3.141593,0,M);
  }
  

}
