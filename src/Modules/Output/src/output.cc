

// output.cc
//  
// Made by Pablo Galaviz
// e-mail  <Pablo.Galaviz@me.com>
// 


// PASTE YOUR LICENCE HERE



#include "output.h"


output::output(string output_directory,bool _verbose, bool _debug){


  tStart = clock();

  verbose=_verbose;
  
  debug=_debug;
  

  
  
  BOOST_LOG_SEV(lg, logging::trivial::info) << "----------- output setup -------------";

  file_name = output_directory+"/output.h5";

  hid_t plist_id = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(plist_id, MPI::COMM_WORLD, MPI::INFO_NULL);  
  file_id = H5Fcreate(file_name.c_str(),H5F_ACC_TRUNC,H5P_DEFAULT,plist_id);
  H5Pclose(plist_id);

  monitor_file_name = output_directory+"/monitor.gp";

  if(debug)
    BOOST_LOG_SEV(lg, logging::trivial::warning) << "debugging output activated";

  if(verbose)
    BOOST_LOG_SEV(lg, logging::trivial::warning) << "verbose output activated";


  /* Close the file. */
  herr_t status = H5Fclose(file_id);

  
}





void output::init()
{


  BOOST_LOG_SEV(lg, logging::trivial::info) << "------------ output init -------------";

  int mpi_rank = MPI::COMM_WORLD.Get_rank ();
  int mpi_size = MPI::COMM_WORLD.Get_size ();

  
  /* Close the file. */
  //  herr_t status = H5Fflush(file_id,H5F_SCOPE_LOCAL);
  
  
  MPI::COMM_WORLD.Barrier ();

  
}


