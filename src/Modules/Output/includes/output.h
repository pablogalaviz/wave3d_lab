

// output.h
//  
// Made by Pablo Galaviz
// e-mail  <Pablo.Galaviz@me.com>
// 



// PASTE LICENCE HERE


#ifndef OUTPUT_H
#define OUTPUT_H

#include<utils.h>
#include<hdf5.h>


class output
{

  src::severity_logger< logging::trivial::severity_level > lg;

  clock_t tStart;

  bool verbose;
  
  bool debug;
  
  
  string file_name;

  string monitor_file_name;
  
  hid_t file_id;


  
 public:

 output(string output_directory,bool _verbose, bool _debug);

 ~output(){};

 
 void init();
  
 inline void close(){
   //hsize_t status = H5Fclose(file_id);
 }
 

};


#endif
