/*

   Title: Instalation


   Download the code from:

> git clone https://github.com/code_repository.git



   Function: main

   Controls the main loop of the code.

   Parameters:

      help,h				- Shows a help message (type: flag | default: false).
      input_file 			- Is a <Parameter File> (type: string | default None).
      silent,s				- Starts the program in silent mode (type: flag | default: false).
      					  Output to the log file exclusively.  
      debug,d 				- Shows additional debug messages in log (type: flag | default: false).


   Returns:

      Zero if the program terminates normally. One otherwise 

   See Also:



    Title: Parameter File

    A parameters file has the following format:

>[output]
>
> directory=waveData
>
> verbose=no

	*Note:* Every parameter is optional. The values in the file are overwritten by the command line values. 

*/
      
