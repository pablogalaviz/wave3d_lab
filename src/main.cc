//  
// Made by Pablo Galaviz
// e-mail  <Pablo.Galaviz@me.com>
// 



// PASTE YOUR LICENCE HERE

#include <main.h>



int main(int ac, char*av[])

{


  
  clock_t tStart = clock();
  
  
  
  try {

    MPI::Init ();


    po::options_description genericOptions("Wave3d Lab \nAllowed options");

    genericOptions.add_options()
      ("help,h", "Shows a help message")
      ("input_file", po::value<string>(), "Is a parameter file")
      ("silent,s", "Shows additional debug messages in log")
      ("debug,d", "Shows additional debug messages in log");

    string output_directory;
    bool output_debug;
    bool output_verbose;

    po::options_description outputOptions("Output options");    
    outputOptions.add_options()
      ("output.directory,o", po::value<string>(&output_directory)->default_value("output"), "set output directory")
      ("output.debug", po::value<bool>(&output_debug)->default_value(false),"output aditional fields (debugging)")
      ("output.verbose", po::value<bool>(&output_verbose)->default_value(false),"output additional information");
    
    
    po::positional_options_description p;
    p.add("input_file", -1);

    
    po::options_description cmdline_options;
    cmdline_options.add(genericOptions).add(outputOptions);

    po::options_description config_file_options;
    config_file_options.add(outputOptions);


    po::variables_map vm;
    po::store(po::command_line_parser(ac, av).options(cmdline_options).positional(p).run(), vm);
    po::notify(vm);


    
    if (vm.count("help") || vm.count("input_file") == 0) {

      if (MPI::COMM_WORLD.Get_rank () == MPI_ROOT_NODE)
	{
	cout << cmdline_options << "\n";
	  if(vm.count("input_file") == 0 && vm.count("help")==0)
	    cout  << endl <<  "MISSING PARAMETER FILE!" << endl << endl;
	}
      return finalize();
    }

    if (vm.count("input_file")){

      string input_file = vm["input_file"].as<string>();

      ifstream ifs(input_file.c_str());
      if (!ifs)
	{
	  cout << "can not open config file: " << input_file << "\n";
	  return 0;
	}
      else
	{
	  store(parse_config_file(ifs, config_file_options), vm);
	  notify(vm);
	}

    }


    make_output_directory(output_directory);

    string cmd = "echo '#!/bin/bash' > " + output_directory + "/command.sh";
    int sys_out = system(cmd.c_str());

    cmd = "echo cd `pwd` >> "  + output_directory + "/command.sh";
    sys_out = system(cmd.c_str());
    
    stringstream param;
    
    for (int i = 0; i < ac; i++)

      param << av[i] << " ";

    cmd = "echo " + param.str() + " >> " + output_directory + "/command.sh;";
    sys_out = system(cmd.c_str());

    
    cmd = "chmod +x " + output_directory + "/command.sh";
    sys_out = system(cmd.c_str());

    
    setup_log(output_directory,vm.count("silent") > 0,vm.count("debug") > 0 );

    using namespace logging::trivial;
    src::severity_logger< severity_level > lg;


    output my_output(output_directory,output_verbose, output_debug);    
    

    my_output.init();

    Wave my_wave(10,10,10);

    my_wave.update();
    
    //main code loop 
    //do{      
    //}while(proceed);

    my_output.close();
	
    
  }
  catch(exception& e) {
    cerr << "error: " << e.what() << "\n";
    finalize();
    return 1;
  }
  catch(...) {
    cerr << "Exception of unknown type!\n";
  }

  using namespace logging::trivial;
  src::severity_logger< severity_level > lg;

  
  double ttime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

  int thour = int(ttime /3600.0);
  int tmin = int( (ttime - thour*3600 )/60.0 );
  double tsec = ttime - thour*3600 - tmin*60;

  
  BOOST_LOG_SEV(lg, info) << "All done. Total execution time: "<< thour << " h " << tmin << " m " << tsec << " s" ;


  return finalize();


}
