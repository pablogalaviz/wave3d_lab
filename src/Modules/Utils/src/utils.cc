

// utils.cc
//  
// Made by Pablo Galaviz
// e-mail  <Pablo.Galaviz@me.com>
// 


// PLACE LICENCE HERE



#include "utils.h"



string code_logo(){

  stringstream result;


  result << endl
	 << "----------------------------------------------------------------------------------------------------" << endl
	 << "____    __    ____  ___   ____    ____  _______ ____    _______      __          ___      .______   " << endl
	 << "\\   \\  /  \\  /   / /   \\  \\   \\  /   / |   ____|___ \\  |       \\    |  |        /   \\     |   _  \\  " << endl
	 << " \\   \\/    \\/   / /  ^  \\  \\   \\/   /  |  |__    __) | |  .--.  |   |  |       /  ^  \\    |  |_)  | " << endl
	 << "  \\            / /  /_\\  \\  \\      /   |   __|  |__ <  |  |  |  |   |  |      /  /_\\  \\   |   _  <  " << endl
	 << "   \\    /\\    / /  _____  \\  \\    /    |  |____ ___) | |  '--'  |   |  `----./  _____  \\  |  |_)  | " << endl
	 << "    \\__/  \\__/ /__/     \\__\\  \\__/     |_______|____/  |_______/    |_______/__/     \\__\\ |______/  " << endl
	 << "                                                                                                    " << endl
	 << "----------------------------------------------------------------------------------------------------" << endl
	 << endl
	 << endl;

  result << "                   =======  wave3d lab 7.17  =======" << endl
         << endl
         << "                   Author: Pablo Galaviz   " << endl
         << endl
         << "                   Email:  Pablo.Galaviz@me.com  " << endl
         << endl
         << "                   =================================" << endl
         << endl;



  return result.str();



}



int finalize(int e){

  MPI::COMM_WORLD.Barrier ();

  MPI::Finalize ();

  return e;

}


void setup_log(string dir_name, bool silent, bool debug){

  int mpi_rank = MPI::COMM_WORLD.Get_rank ();

  string log_file = "/output"+boost::lexical_cast<std::string>(mpi_rank)+".log";

  logging::add_file_log
    (
     keywords::file_name = dir_name+log_file,                                        /*< file name pattern >*/
     keywords::rotation_size = 100 * 1024 * 1024,                                   /*< rotate files every 100 MiB... >*/
     keywords::format =  (expr::stream
			  << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S")
			  << "Wave3d lab [cpu "<< mpi_rank << " " << logging::trivial::severity
			  << "] | " << expr::smessage),                            /*< log record format >*/
     boost::log::keywords::auto_flush = true
     );

  if(!silent && mpi_rank == MPI_ROOT_NODE)
    logging::add_console_log(cout, keywords::format = (expr::stream
						       << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S")
						       << " Wave3d Lab [cpu "<< mpi_rank << " " << logging::trivial::severity
						       << "] | " << expr::smessage)  );

  if(debug)
    logging::core::get()->set_filter
      (
       logging::trivial::severity >= logging::trivial::debug
       );
  else
    logging::core::get()->set_filter
      (
       logging::trivial::severity >= logging::trivial::info
       );


  logging::add_common_attributes();

  BOOST_LOG_TRIVIAL(info) << code_logo();
  BOOST_LOG_TRIVIAL(info) << "Project directory: "<< dir_name;


}


void make_output_directory(string dir_name){

  if(MPI::COMM_WORLD.Get_rank () == MPI_ROOT_NODE)
    {

      string rmdir = "rm -rf " + dir_name + "_prev";


      int sys_out = system(rmdir.c_str());



      string mvdir = "mv -f " + dir_name + " " + dir_name + "_prev";


      sys_out = system(mvdir.c_str());


      string mkdir = "mkdir " + dir_name;


      sys_out = system(mkdir.c_str());
    }

  MPI::COMM_WORLD.Barrier ();


}
